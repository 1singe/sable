using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using System;

[Serializable, VolumeComponentMenu("Post-processing/PaintEffect/Kuwahara")]
public sealed class Kuwahara : CustomPostProcessVolumeComponent, IPostProcessComponent
{
    [Tooltip("Controls the intensity of the effect.")]
    public ClampedFloatParameter Intensity = new ClampedFloatParameter(1, 0, 1);
    public ClampedIntParameter range = new ClampedIntParameter(10,1,100);
    
    public Vector2Parameter sampleSize = new Vector2Parameter(Vector2.one);
    public Vector2Parameter direction = new Vector2Parameter(Vector2.zero);
    [Tooltip("Skipping steps can reduce performance cost")]
    public Vector2Parameter stepSkip = new Vector2Parameter(Vector2.one);
    public BoolParameter keepSmallObject = new BoolParameter(true);
    public FloatParameter smallObjectThresold = new FloatParameter(0.0001f);

    Material m_Material;

    public bool IsActive() => m_Material != null && Intensity.value > 0;

    public override CustomPostProcessInjectionPoint injectionPoint => CustomPostProcessInjectionPoint.AfterPostProcess;

    public override void Setup()
    {
        if (Shader.Find("Hidden/Shader/Kuwahara") != null)
            m_Material = new Material(Shader.Find("Hidden/Shader/Kuwahara"));
    }

    public override void Render(CommandBuffer cmd, HDCamera camera, RTHandle source, RTHandle destination)
    {
        if (m_Material == null || Intensity.value <= 0)
            return;

        /*
        Vector2 v = stepSizeParameter.value;
        v.x = Mathf.Clamp(v.x, 1,  Mathf.Min(rangeParameter.value-1,1));
        v.y = Mathf.Clamp(v.y, 1,  Mathf.Min(rangeParameter.value-1,1));
        stepSizeParameter.value = v;*/
        float vx = Mathf.Clamp(stepSkip.value.x, 0.1f, Mathf.Max(range.value - 1, 1));
        float vy = Mathf.Clamp(stepSkip.value.y, 0.1f, Mathf.Max(range.value - 1, 1));
        Vector2 v = new Vector2(vx, vy);
        stepSkip.value = v;
     
        m_Material.SetFloat("_Intensity", Intensity.value);
        m_Material.SetInt("_Range", Mathf.Max(range.value, 1));
        m_Material.SetTexture("_InputTexture", source);
        m_Material.SetVector("_StepSize", sampleSize.value);
        m_Material.SetVector("_Dir", direction.value);
        m_Material.SetVector("_StepOffset", v);
        m_Material.SetInt("_KeepObject", keepSmallObject.value?0:1);
        m_Material.SetFloat("_ObjThre", smallObjectThresold.value);
        HDUtils.DrawFullScreen(cmd, m_Material, destination);
    }

    public override void Cleanup() => CoreUtils.Destroy(m_Material);

}