using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.Rendering;
using UnityEngine.Rendering.RendererUtils;
using Random = System.Random;

class EdgesOutlinePass : CustomPass
{



    public RenderTexture normalRT;

    
    public ClampedFloatParameter _Intensity = new ClampedFloatParameter(1, 0, 1);
    public ClampedFloatParameter _ThresholdDepth = new ClampedFloatParameter(1, 0.1f, 10f);
    public ClampedFloatParameter _ThresholdNormal = new ClampedFloatParameter(1, 0.1f, 10f);
    public ClampedFloatParameter _ThresholdDepthNormal = new ClampedFloatParameter(1, 0.1f, 10f);
    public ClampedFloatParameter _DepthNormalScale = new ClampedFloatParameter(1, 0.1f, 10f);
    public ClampedFloatParameter _Ratio = new ClampedFloatParameter(1, 0f, 1f);
    public ClampedFloatParameter _Size = new ClampedFloatParameter(5, 1, 100);
    
    
    private Shader fullscreenShader;
    private Material fullscreenMaterial;
    

    // It can be used to configure render targets and their clear state. Also to create temporary render target textures.
    // When empty this render pass will render to the active camera render target.
    // You should never call CommandBuffer.SetRenderTarget. Instead call <c>ConfigureTarget</c> and <c>ConfigureClear</c>.
    // The render pipeline will ensure target setup and clearing happens in an performance manner.
    protected override void Setup(ScriptableRenderContext renderContext, CommandBuffer cmd)
    {
        // Setup code here
        fullscreenShader = Shader.Find("Hidden/Shader/EdgesOutlinePass");
        fullscreenMaterial = CoreUtils.CreateEngineMaterial(fullscreenShader);
        




        Debug.Assert(fullscreenMaterial != null, "Failed to create fullscreen pass material");

    }

    public  int FindCamIndex( List<Camera> list, Camera obj)
    {
        for (int i = 0; i < list.Count; i++)
        {
            if (obj == list[i])
                return i;
        }

        return 0;
    }

    public CompareFunction compareFunction;
    protected override void Execute(CustomPassContext ctx)
    {
        if (ctx.hdCamera.camera.cameraType != CameraType.Game)
            return;

      //  SyncRenderTextureAspect(normalRT,ctx.hdCamera.camera);
        
        /*
        CoreUtils.SetRenderTarget(ctx.cmd, normalRT, ClearFlag.All);
        RenderStateBlock block = new RenderStateBlock();
        block.depthState = new DepthState(false, compareFunction);*/
        
        
        
        /*
        PerObjectData renderConfig = ctx.hdCamera.frameSettings.IsEnabled(FrameSettingsField.Shadowmask) ? HDUtils.GetBakedLightingWithShadowMaskRenderConfig() : HDUtils.GetBakedLightingRenderConfig();
        ctx.hdCamera.camera.TryGetCullingParameters(out var cullingParameters);

        // Use the culling parameters to perform a cull operation, and store the results
        var cullingResults = ctx.renderContext.Cull(ref cullingParameters);
        
        var result = new RendererListDesc(new ShaderTagId(), cullingResults, ctx.hdCamera.camera)
        {
            rendererConfiguration = renderConfig,
            renderQueueRange = GetRenderQueueRange(renderQueue),
            sortingCriteria = normalSorting,
            excludeObjectMotionVectors = false,
            overrideMaterial = normalMat,
            overrideMaterialPassIndex = normalPass,
            stateBlock = block,
            layerMask = Mask,
        };

        var renderCtx = ctx.renderContext;
        CoreUtils.DrawRendererList(ctx.renderContext, ctx.cmd, renderCtx.CreateRendererList(result));
        */
        
        
        
        //CustomPassUtils.DrawRenderers(ctx, Mask, renderQueue, normalMat, normalPass, block, normalSorting);


        /*
        RenderTexture customRT = ctx.customColorBuffer.Value.rt;
        normalRT = new RenderTexture(customRT.width, customRT.height, customRT.depth, customRT.format);
        normalRT.Create();*/
       // convertMat.SetTexture("_Input", ctx.customColorBuffer.Value);
        //normalRT = new RenderTexture(ctx.customColorBuffer.Value.rt);
      //  Debug.Log(ctx.customColorBuffer.Value.rt.width + " / " + normalRT.width);
       // ctx.cmd.CopyTexture(normalRT, normalRT);
       // ctx.cmd.Blit(ctx.customColorBuffer.Value.rt, normalRT, new Vector2(1, 1), Vector2.zero);

       // Outiline
        
        CoreUtils.SetRenderTarget(ctx.cmd, ctx.cameraColorBuffer, ClearFlag.None);

        Matrix4x4 clipToView = GL.GetGPUProjectionMatrix(ctx.hdCamera.camera.projectionMatrix, true).inverse;
        
        fullscreenMaterial.SetFloat("_Intensity", _Intensity.value);
        fullscreenMaterial.SetFloat("_ThresholdDepth", _ThresholdDepth.value);
        fullscreenMaterial.SetFloat("_ThresholdNormal", _ThresholdNormal.value);
        fullscreenMaterial.SetFloat("_ThresholdDepthNormal", _ThresholdDepthNormal.value);
        fullscreenMaterial.SetFloat("_DepthNormalScale", _DepthNormalScale.value);
        fullscreenMaterial.SetFloat("_Ratio", _Ratio.value);
        fullscreenMaterial.SetFloat("_Size", _Size.value);
        fullscreenMaterial.SetTexture("_NormalTexture", normalRT);
        fullscreenMaterial.SetTexture("_InputNormal", ctx.customColorBuffer.Value.rt);
        fullscreenMaterial.SetMatrix("_ClipToView", clipToView);
        
        CoreUtils.DrawFullScreen(ctx.cmd, fullscreenMaterial, ctx.propertyBlock, shaderPassId: 0);
        
    }
    
    void SyncRenderTextureAspect(RenderTexture rt, Camera camera)
    {
        float aspect = rt.width / (float)rt.height;

        if (!Mathf.Approximately(aspect, camera.aspect))
        {
            rt.Release();
            rt.width = camera.pixelWidth;
            rt.height = camera.pixelHeight;
            rt.Create();
        }
    }

    protected override void Cleanup()
    {
        // Cleanup code
        CoreUtils.Destroy(fullscreenMaterial);
    }
}