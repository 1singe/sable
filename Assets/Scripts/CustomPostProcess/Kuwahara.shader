Shader "Hidden/Shader/Kuwahara"
{
    HLSLINCLUDE

    #pragma target 4.5
    #pragma only_renderers d3d11 playstation xboxone xboxseries vulkan metal switch

    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
    #include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderVariables.hlsl"
    #include "Packages/com.unity.render-pipelines.high-definition/Runtime/PostProcessing/Shaders/FXAA.hlsl"
    #include "Packages/com.unity.render-pipelines.high-definition/Runtime/PostProcessing/Shaders/RTUpscale.hlsl"

    struct Attributes
    {
        uint vertexID : SV_VertexID;
        UNITY_VERTEX_INPUT_INSTANCE_ID
    };

    struct Varyings
    {
        float4 positionCS : SV_POSITION;
        float2 texcoord   : TEXCOORD0;
        UNITY_VERTEX_OUTPUT_STEREO

    };

    Varyings Vert(Attributes input)
    {
        Varyings output;

        UNITY_SETUP_INSTANCE_ID(input);
        UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

        output.positionCS = GetFullScreenTriangleVertexPosition(input.vertexID);
        output.texcoord = GetFullScreenTriangleTexCoord(input.vertexID);

        return output;
    }



    // List of properties to control your post process effect
    float _Intensity;
    int _Range;
    int _KeepObject;
    float _ObjThre;
    float2 _StepSize;
    float2 _Offset;
    float2 _Dir;
    float2 _StepOffset;
    TEXTURE2D_X(_InputTexture);
    TEXTURE2D(_DepthTexture);


    float4 Mix(float3 A, float3 B, float t)
    {
        float x = lerp(A.x, B.x, t);
        float y = lerp(A.y, B.y, t);
        float z = lerp(A.z, B.z, t);
        return float4(x,y,z,1);
    }


   float3 GetMean(float2 dir, uint2 uv)
    {
        
        float3 sum = float4(0, 0, 0, 0);
        int count = 0;
       float depth = 0;

        for (int x = 0; x < _Range; x+=_StepOffset.x)
        {
            for (int y = 0; y < _Range; y+=_StepOffset.y)
            {
                float posX = uv.x + (x * _StepSize.x * (dir.x+_Dir.x))+_Offset.x;
                float posY = uv.y + (y * _StepSize.y * (dir.y+_Dir.y))+_Offset.y;

                uint2 pos = uint2(round(posX), round(posY));

                float3 color = LOAD_TEXTURE2D_X(_InputTexture, pos).xyz;
                float _depth = LoadCameraDepth(pos);
                float d= 1/LinearEyeDepth(_depth, _ZBufferParams);
                if (depth == 0 || _KeepObject == 1)
                {
                    depth = d;
                }else if (abs(depth-d)>_ObjThre)
                {
                    continue;
                }
                
                sum += color;
                count++;
            }
        }

        return sum / (float)count;

    }

    float GetVariance(float2 dir, uint2 uv, float3 mean)
    {
        
        float _variance = 0;

        for (int x = 0; x < _Range; x+=_StepOffset.x)
        {
            for (int y = 0; y < _Range; y+=_StepOffset.y)
            {
                float posX = uv.x + (x * _StepSize.x * (dir.x + _Dir.x));
                float posY = uv.y + (y * _StepSize.y * (dir.y + _Dir.y));

                uint2 pos = uint2(round(posX), round(posY));

                float3 color = LOAD_TEXTURE2D_X(_InputTexture, pos).xyz;
                float _var = pow(color.x-mean.x+color.y-mean.y+color.z-mean.z,2);
		        _variance += _var;
            }
        }

        return _variance;

    }

    float4 CustomPostProcess(Varyings input) : SV_Target
    {
        UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

        uint2 uv = input.texcoord * _ScreenSize.xy;
        float3 baseColor = LOAD_TEXTURE2D_X(_InputTexture, uv).xyz;
        float depth = LoadCameraDepth(uv);
        float linearEyeDepth = 1/LinearEyeDepth(depth, _ZBufferParams);

        float4 color = (1,0,1,1);

        float3 meanNW = GetMean(float2(-1,1), uv);
        float3 meanNE = GetMean(float2(1,1), uv);
        float3 meanSE = GetMean(float2(1,-1), uv);
        float3 meanSW = GetMean(float2(-1,-1), uv);

        float varNW = GetVariance(float2(-1,1), uv, meanNW);
        float varNE = GetVariance(float2(1,1), uv, meanNE);
        float varSE = GetVariance(float2(1,-1), uv, meanSE);
        float varSW = GetVariance(float2(-1,-1), uv, meanSW);

        float minVar = min(varNW, min(varNE, min(varSE, varSW)));

        if (minVar == varNW)
        {
            color.xyz = meanNW;
        }else if(minVar == varNE)
        {
            color.xyz = meanNE;
        }else if (minVar == varSE)
        {
            color.xyz = meanSE;
        }else if (minVar == varSW)
        {
            color.xyz = meanSW;
        }
        

        // We do the blending manually instead of relying on the hardware blend
        // It's necessary because the color buffer contains garbage from the previous post process effect.
        //color.xyz = meanNW;
        return Mix(baseColor.xyz, color.xyz, _Intensity);
    }



    ENDHLSL

    SubShader
    {
        Pass
        {
            Name "Kuwahara"

            ZWrite Off
            ZTest Always
            Blend Off
            Cull Off

            HLSLPROGRAM
                #pragma fragment CustomPostProcess
                #pragma vertex Vert
            ENDHLSL
        }
    }

    Fallback Off
}