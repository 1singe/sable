Shader "Hidden/Shader/EdgesOutlinePass"

{

    HLSLINCLUDE

    #pragma target 4.5

    #pragma only_renderers d3d11 ps4 xboxone vulkan metal switch

    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"

    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"

    #include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderVariables.hlsl"

    #include "Packages/com.unity.render-pipelines.high-definition/Runtime/PostProcessing/Shaders/FXAA.hlsl"

    #include "Packages/com.unity.render-pipelines.high-definition/Runtime/PostProcessing/Shaders/RTUpscale.hlsl"

    struct Attributes

    {

        uint vertexID : SV_VertexID;

        UNITY_VERTEX_INPUT_INSTANCE_ID

    };

    struct Varyings

    {

        float4 positionCS : SV_POSITION;

        float2 texcoord   : TEXCOORD0;

        float3 viewSpaceDir : TEXCOORD2;

        UNITY_VERTEX_OUTPUT_STEREO

    };

    float4x4 _ClipToView;
    
    Varyings Vert(Attributes input)

    {

        Varyings output;

        UNITY_SETUP_INSTANCE_ID(input);

        UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

        output.positionCS = GetFullScreenTriangleVertexPosition(input.vertexID);

        output.texcoord = GetFullScreenTriangleTexCoord(input.vertexID);

        output.viewSpaceDir = mul(_ClipToView, output.positionCS).xyz;

        return output;

    }

    float getDepth(float2 pos)
    {
        pos=pos*_ScreenSize.xy;
        float _depth = LoadCameraDepth(pos);
        float d= 1/LinearEyeDepth(_depth, _ZBufferParams);
        return d;
    }

    float _Intensity;
    float _Size;

    float _ThresholdDepth;
    float _ThresholdNormal;
    float _ThresholdDepthNormal;
    float _DepthNormalScale;
    float _Ratio;

    TEXTURE2D_ARRAY(_InputNormal);
   SAMPLER(sampler_InputNormal);
    


    sampler2D _NormalTexture;

     float Remap(float value, float from1, float to1, float from2, float to2)
     {
            return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
     }

    float3 getNorm(float2 pos)
    {
         //float4 norm = SAMPLE_TEXTURE2D_ARRAY(_InputNormal, sampler_InputNormal, pos, 0);
        float3 norm = tex2D(_NormalTexture, pos);
         return 1-norm.xyz;
        norm.x = Remap(norm.x, 0.0,1.0, -1.0,1.0 );
        norm.y= Remap(norm.y, 0.0,1.0, -1.0,1.0 );
        norm.z = Remap(norm.z, 0.0,1.0, -1.0,1.0 );

         norm.x = 0.5+norm.x*0.5;
         norm.y = 0.5+norm.y*0.5;
         norm.z = 0.5+norm.z*0.5;
        return norm;
    }


    // List of properties to control your post process effect

    


    TEXTURE2D_X(_InputTexture);

    float4 CustomPostProcess(Varyings input) : SV_Target

    {

        UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);
     
        uint2 pos = input.texcoord * _ScreenSize.xy;
        float xScale = 1.0/_ScreenSize.x;
        float yScale = 1.0/_ScreenSize.y;

        float2 top=input.texcoord+(0,_Size*yScale);
        float2 bot=input.texcoord-(0,_Size*yScale);
        float2 right=input.texcoord+(_Size*xScale,0);
        float2 left=input.texcoord-(_Size*xScale,0);

        float2 topRight=input.texcoord+(_Size*xScale, _Size*yScale);
        float2 botLeft=input.texcoord-(_Size*xScale, _Size*yScale);
        float2 topLeft=input.texcoord+(-_Size*xScale, _Size*yScale);
        float2 botRight=input.texcoord+(_Size*xScale, -_Size*yScale);

        float depth0 = getDepth(botLeft);
        float depth1 = getDepth(topRight);
        float depth2 = getDepth(botRight);
        float depth3 = getDepth(topLeft);

        float depthFiniteDifference0 = depth1 - depth0;
        float depthFiniteDifference1 = depth3 - depth2;


         float3 normal0 = getNorm(botLeft);
        float3 normal1 = getNorm(topRight);
        float3 normal2 = getNorm(botRight);
        float3 normal3 = getNorm(topLeft);
        float3 normCenter = getNorm(input.texcoord);

        float dot0 = dot(normal0, normCenter);
        float dot1 = dot(normal1, normCenter);
        float dot2 = dot(normal2, normCenter);
        float dot3 = dot(normal3, normCenter);

        float _dot = min(min(dot1, min(dot2, dot3)), dot0);
        _dot = pow(_dot-0.5, 5);
        _dot = clamp(_dot, 0,1);
        _dot = pow(_dot, 5);
       // _dot += 1+depth0;

        float3 normalFiniteDifference0 = normal1 - normal0;
        float3 normalFiniteDifference1 = normal3 - normal2;


        
        

        float edgeDepth = sqrt(pow(depthFiniteDifference0, 3) + pow(depthFiniteDifference1, 3)) * 100;
        edgeDepth = edgeDepth > _ThresholdDepth*(depth0) ? 1 : 0;	

        float3 baseColor = LoadCameraColor(input.positionCS.xy, 0);



       


        

        float edgeNormal = sqrt(dot(normalFiniteDifference0, normalFiniteDifference0) + dot(normalFiniteDifference1, normalFiniteDifference1));
        edgeNormal = edgeNormal > _ThresholdNormal ? 1 : 0;

        
        //baseColor = tex2D(_NormalTexture, right);

        //edgeDepth*=_Ratio;
        //edgeNormal*=1-_Ratio;
        float edge = max(edgeDepth, edgeNormal);

        float3 v = baseColor*(1-edge);
        v= baseColor * _dot;
       // v=_dot;
       // v= normal0;

        return float4(lerp(baseColor, v, _Intensity), 1);
        //return float4(edgeNormal, edgeNormal, edgeNormal, 1);

    }

    ENDHLSL

    SubShader

    {

        Pass

        {

            Name "Edges Outline Pass"

            ZWrite Off

            ZTest Always

            Blend Off

            Cull Off

            HLSLPROGRAM

                #pragma fragment CustomPostProcess

                #pragma vertex Vert

            ENDHLSL

        }

    }

    Fallback Off

}
