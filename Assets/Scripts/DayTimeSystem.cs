using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;


public class DayTimeSystem : MonoBehaviour
{

    [Range(0,23)]
    public int hour = 0;
    [Range(0,59)]
    public float min = 0;

    [Range(0,1f)]
    public float currentTime = 0;

    public bool gatherTimeData = false;
    public float autoTime = 0;

    public Light mainLight;
    public AnimationCurve LightIntensity;
    public float maxIntensity;
    

    public DayTimeVolume[] skyVolumes;
    public DayTimeParticleSystem[] particleSystems;
    public DayTimeParticleGameObject[] gameObjects;
    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnValidate()
    {
        //Update();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateTime();
        UpdateVolume();
        UpdateParticles();
        UpdateGameObject();
        UpdateLight();
    }

    public void UpdateTime()
    {
        if (gatherTimeData)
        {
            hour = DateTime.Now.Hour;
            min = DateTime.Now.Minute;
        }

        min += autoTime * Time.deltaTime * 100;

        if (min >= 60)
        {
            hour++;
            min -= 60;
        }else if (min < 0)
        {
            hour--;
            min += 60;
        }

        if (hour >= 24)
        {
            hour = 0;
        }else if (hour < 0)
        {
            hour = 23;
        }
        currentTime = (hour * 60 + min) / 1440f;
    }

    public void UpdateVolume()
    {
        int length = skyVolumes.Length;
        
        for (int i = 0; i < length; i++)
        {
            skyVolumes[i].volume.weight = skyVolumes[i].weightOverTime.Evaluate(currentTime);
        }
        
    }

    public void UpdateParticles()
    {
        for (int i = 0; i < particleSystems.Length; i++)
        {
            DayTimeParticleSystem system = particleSystems[i];

            if (currentTime > system.minTimeActive && currentTime < system.maxTimeActive )
            {
                if (!system.system.isPlaying)
                    system.system.Play();
            }
            else
            {
                if (system.system.isPlaying)
                    system.system.Stop();
            }
            
 
        }
    }
    
    public void UpdateGameObject()
    {
        for (int i = 0; i < gameObjects.Length; i++)
        {
            DayTimeParticleGameObject system = gameObjects[i];

            if (currentTime > system.minTimeActive && currentTime < system.maxTimeActive )
            {
                system.obj.SetActive(true);
            }
            else
            {
                system.obj.SetActive(false);
            }
            
 
        }
    }

    public void UpdateLight()
    {
        Quaternion rotation = mainLight.transform.rotation;
        rotation = Quaternion.AngleAxis(90+currentTime*180f, Vector3.forward)*Quaternion.AngleAxis(-90, Vector3.right);
        mainLight.transform.rotation = rotation;
        float intensity = LightIntensity.Evaluate(currentTime) * maxIntensity;
        mainLight.intensity = intensity;
        mainLight.GetComponent<HDAdditionalLightData>().intensity = intensity;
    }
        
    [System.Serializable]
    public struct  DayTimeVolume
    {
        [SerializeField]
        public Volume volume;
        [SerializeField]
        public AnimationCurve weightOverTime;
    }
    
    [System.Serializable]
    public struct DayTimeParticleSystem
    {
        [SerializeField] public ParticleSystem system;
        [SerializeField] public float minTimeActive;
        [SerializeField] public float maxTimeActive;
    }
    
    [System.Serializable]
    public struct DayTimeParticleGameObject
    {
        [SerializeField] public GameObject obj;
        [SerializeField] public float minTimeActive;
        [SerializeField] public float maxTimeActive;
    }
}
