﻿using UnityEngine;

namespace KinectBody
{
    public interface IInitializable
    {
        bool IsInitializationComplete { get; }
        bool IsAvailable { get; }
    }
}
