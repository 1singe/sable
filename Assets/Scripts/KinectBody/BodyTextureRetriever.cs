using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using K4AdotNet;
using K4AdotNet.BodyTracking;
using K4AdotNet.Sensor;
using UnityEngine;
using UnityEngine.UI;
using Image = K4AdotNet.Sensor.Image;

namespace KinectBody
{
    public class BodyTextureRetriever : MonoBehaviour
    {
        public Material CustomShaderMaterial;
        
        public Tracker _tracker;
        public bool IsInitializationComplete { get; private set; }
        public bool IsAvailable { get; private set; }

        public Texture2D bodyTexture;
        private RenderTexture _renderTexture;
        public RawImage rawImage;

        private IEnumerator Start()
        {
            yield return new WaitForSeconds(2);

            var task = Task.Run(() =>
            {
                var initialized = Sdk.TryInitializeBodyTrackingRuntime(TrackerProcessingMode.GpuCuda, out var message);
                return Tuple.Create(initialized, message);
            });
            yield return new WaitUntil(() => task.IsCompleted);

            var isAvailable = false;
            try
            {
                var result = task.Result;
                isAvailable = result.Item1;
                if (!isAvailable)
                {
                    Debug.Log($"Cannot initialize body tracking: {result.Item2}");
                }
            }
            catch (Exception ex)
            {
                Debug.LogWarning($"Exception on {nameof(Sdk.TryInitializeBodyTrackingRuntime)}\r\n{ex}");
            }

            if (isAvailable)
            {
                var captureManager = FindObjectOfType<CaptureManager>();
                yield return new WaitUntil(() => captureManager?.IsInitializationComplete != false);
                if (captureManager?.IsAvailable == true)
                {
                    var calibration = captureManager.Calibration;
                    
                    var config = TrackerConfiguration.Default;
                    config.ProcessingMode = TrackerProcessingMode.GpuCuda;
                    // Use lite version of DNN model for speed (comment next line to use default DNN model)
                    config.ModelPath = Sdk.BODY_TRACKING_DNN_MODEL_LITE_FILE_NAME;

                    _tracker = new Tracker(in calibration, config);
                    
                    
                    var frameWidth = captureManager.Configuration.DepthMode.WidthPixels();
                    var frameHeight = captureManager.Configuration.DepthMode.HeightPixels();
                    bodyTexture.Reinitialize(frameWidth, frameHeight, TextureFormat.Alpha8, false);
                   // bodyTexture = new Texture2D(frameWidth, frameHeight, TextureFormat.Alpha8, false);
                    
                    //this render texture is to be used with a BLIT
                    /*
                    _renderTexture = new RenderTexture(frameWidth, frameHeight, 0);
                    _renderTexture.Create();
                    GetComponent<RawImage>().texture = _renderTexture;
                    GetComponent<AspectRatioFitter>().aspectRatio = (float)frameWidth / frameHeight;
                    */
                    
                    captureManager.CaptureReady += CaptureManager_CaptureReady;
                }
                else
                {
                    isAvailable = false;
                }
            }

            
            IsAvailable = isAvailable;
            IsInitializationComplete = true;
        }
        
        private void CaptureManager_CaptureReady(object sender, CaptureEventArgs e)
        {

            if (IsAvailable)
            {
                using var capture = e.Capture;
                using var depthImage = capture.DepthImage;
                using var irImage = capture.IRImage;
                if (!(depthImage is null) && !(irImage is null))
                    _tracker.TryEnqueueCapture(capture);
            }
            
            if(_tracker != null && _tracker.TryPopResult(out var baseFrame))
            {
                Image bodyImage = baseFrame.BodyIndexMap;
                byte[] bodyArray = new byte[baseFrame.BodyIndexMap.SizeBytes];
                Marshal.Copy(bodyImage.Buffer, bodyArray, 0, bodyArray.Length);
                bodyTexture.SetPixelData(bodyArray,0);
                bodyTexture.Apply();
                //rawImage.texture = bodyTexture;
            }

            //Insert blit call here to write to the renderTexture
            //Graphics.Blit(bodyTexture, _renderTexture, CustomShaderMaterial);
        }
        
        private void OnDestroy()
        {
            IsAvailable = false;
            _tracker?.Dispose();
        }
    }
}
