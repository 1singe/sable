﻿using K4AdotNet.Sensor;
using System;

namespace KinectBody
{
    public class CaptureEventArgs : EventArgs
    {
        public CaptureEventArgs(Capture capture)
        {
            Capture = capture;
        }

        public Capture Capture { get; }
    }
}